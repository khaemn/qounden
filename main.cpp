#include <iostream>
#include <math.h>
#include <stdio.h>

#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include "wavfile.h"
#include "wavefilegenerator.h"
#include "samplegenerators/sinewavesamplegenerator.h"
#include "samplegenerators/ntonesamplegenerator.h"
#include "samplegenerators/noisesamplegenerator.h"
#include "samplegenerators/randomtonesamplegenerator.h"


using namespace std;

int main(int argc, char * argv[])
{
    static const unsigned int DEFAULT_DURATION_SEC = 120;
    static const unsigned int DURATION_DEVIATION_SEC = 60;
    static const double DEFAULT_FREQUENCY_HZ = 960.;
    static const double FREQUENCY_DEVIATION_HZ = 120.;

    srand(0);

    WaveFileGenerator fileGenerator;

    fileGenerator.setSampleGenerator(std::unique_ptr<AbstractSampleGenerator>(
                                         new RandomToneSampleGenerator(
                                             120.0,
                                             0.4))
                                     );

    if (argc < 2) {
        fileGenerator.generateWavFile("random.wav", 120, 960.);
    } else {
        std::string filecount = argv[1];
        int total_files = std::atoi(filecount.c_str());
        // TODO: error handling
        int fileno = 0;
        while(fileno++ < total_files) {
            std::string fname = "generated_";
            fname.append(std::to_string(fileno));
            fname.append(".wav");
            int duration_deviation = drand48() * DURATION_DEVIATION_SEC - DURATION_DEVIATION_SEC / 2;
            double frequency_deviation = drand48() * FREQUENCY_DEVIATION_HZ - FREQUENCY_DEVIATION_HZ / 2;
            printf("Generating file %d of %d\n", fileno, total_files);
            fileGenerator.generateWavFile(fname,
                                          DEFAULT_DURATION_SEC + duration_deviation,
                                          DEFAULT_FREQUENCY_HZ + frequency_deviation);
        }
    }
    return 0;
}
