#ifndef WAVEGENERATOR_H
#define WAVEGENERATOR_H

#include <memory>
#include <string>

#include "samplegenerators/abstractsamplegenerator.h"


// http://www-mmsp.ece.mcgill.ca/Documents/AudioFormats/WAVE/WAVE.html
struct WavFileHeader {
    char    riff_tag[4];
    int     riff_length;
    char    wave_tag[4];
    char    fmt_tag[4];
    int     fmt_length;
    short   audio_format;
    short   num_channels;
    int     sample_rate;
    int     byte_rate;
    short   block_align;
    short   bits_per_sample;
    char    data_tag[4];
    int     data_length;
};

class WaveFileGenerator
{
public:
    enum class SignalType {
        SINE,
        SQUARE,
        NOISE
    };
    WaveFileGenerator();

    void generateDefaultWavFile();
    void generateWavFile(std::string name,
                         unsigned int duration_sec,
                         double frequency_hz = -1.,
                         double volume = -1. );

    static const unsigned int DEFAULT_SAMPLES_PER_SECOND;
    static const unsigned int DEFAULT_NUM_SAMPLES;
    static const unsigned int DEFAULT_BIT_DEPTH;

    void setSampleGenerator(std::unique_ptr<AbstractSampleGenerator> generator);

private:
    FILE * openWaveFile( const char *filename );
    void writeWaveFile( FILE *file, short data[], unsigned int length );
    void closeWaveFile( FILE * file );

    unsigned int m_samples_per_second = DEFAULT_SAMPLES_PER_SECOND;
    unsigned int m_volume = 32000;

    std::unique_ptr<AbstractSampleGenerator> m_sample_generator = nullptr;
};

#endif // WAVEGENERATOR_H
