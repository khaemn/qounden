#include "wavefilegenerator.h"

#include <iostream>
#include <math.h>
#include <algorithm>

#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include "wavfile.h"

const unsigned int WaveFileGenerator::DEFAULT_SAMPLES_PER_SECOND = 44100;
const unsigned int WaveFileGenerator::DEFAULT_NUM_SAMPLES = (WaveFileGenerator::DEFAULT_SAMPLES_PER_SECOND*10);
const unsigned int WaveFileGenerator::DEFAULT_BIT_DEPTH = 16;

WaveFileGenerator::WaveFileGenerator()
{
    srand(0);
}

void WaveFileGenerator::generateDefaultWavFile()
{
    // TODO: polish or remove this all.
    short waveform[DEFAULT_NUM_SAMPLES];
    double frequency = 440.0;
    int volume = 32000;
    int length = DEFAULT_NUM_SAMPLES;

    int i;
    for(i=0;i<length;i++) {
        double t = (double) i / DEFAULT_SAMPLES_PER_SECOND;
        waveform[i] = volume*sin(frequency*t*2*M_PI);
    }

    FILE * f = openWaveFile("sound.wav");
    if(!f) {
        printf("Couldn't open sound.wav for writing: %s", strerror(errno));
        return;
    }

    writeWaveFile(f, waveform, length);
    closeWaveFile(f);
}

void WaveFileGenerator::generateWavFile(std::string name,
                                        unsigned int duration_sec,
                                        double frequency_hz,
                                        double volume)
{
    Samples tone;

    const unsigned int total_samples = m_samples_per_second * duration_sec;

    if (frequency_hz > 0.) {
        m_sample_generator->setObertoneFrequency(frequency_hz);
    }

    if (volume > -1.) {
        m_sample_generator->setVolume(volume);
    }

    tone = m_sample_generator->getSamples(total_samples);
    m_sample_generator->setObertoneFrequency(frequency_hz *3 / 16);
    Samples bass = m_sample_generator->getSamples(total_samples);

    for (unsigned long sample = 400; sample < tone.size(); ++sample)
    {
        tone[sample] = (tone[sample] + bass[sample-400] * 2
                        + bass[sample / 4 ] * 2
                        + tone[sample / 7] / 2) / 5.5;
    }

    FILE * file = openWaveFile(name.c_str());
    if (!file) {
        printf("Couldn't open sound.wav for writing: %s", strerror(errno));
        return;
    }

    Samples stereo(tone.size() * 2);
    int stereo_offset_samples = 800;
    for (unsigned long sample = 0; sample < tone.size(); ++sample) {
        stereo[2*sample] = tone[sample];
        if (sample > stereo_offset_samples) {
            stereo[2*sample+1] = tone[sample - stereo_offset_samples];
        } else {
            stereo[2*sample+1] = tone[sample] / 2;
        }
    }

    writeWaveFile(file, stereo.data(), stereo.size());
    closeWaveFile(file);
}

FILE* WaveFileGenerator::openWaveFile( const char *filename )
{
    FILE * file = fopen(filename,"wb+");
    if(!file) {
        return nullptr;
    }

    WavFileHeader header;

    short channels = 2;
    int samples_per_second = WAVE_SAMPLES_PER_SECOND;
    int bits_per_sample = 16;

    strncpy(header.riff_tag, "RIFF",    4);
    strncpy(header.wave_tag, "WAVE",    4);
    strncpy(header.fmt_tag,  "fmt ",    4);
    strncpy(header.data_tag, "data",    4);

    header.riff_length = 0;
    header.fmt_length = 16;
    header.audio_format = 1;
    header.num_channels = channels;
    header.sample_rate = samples_per_second;
    header.byte_rate = samples_per_second * channels * bits_per_sample / 8;
    header.block_align = channels * bits_per_sample / 8;
    header.bits_per_sample = bits_per_sample;
    header.data_length = 0;

    fwrite(&header, sizeof(header), 1, file);

    fflush(file);

    return file;

}

void WaveFileGenerator::writeWaveFile( FILE *file, short data[], unsigned int length )
{
    fwrite(data, sizeof(int16_t), length,file);
}

void WaveFileGenerator::closeWaveFile( FILE *file )
{
    const uint32_t file_length = ftell(file);

    const uint32_t data_length = file_length - sizeof(WavFileHeader);
    fseek(file,sizeof(WavFileHeader) - sizeof(int),SEEK_SET);
    fwrite(&data_length,sizeof(data_length),1,file);

    const uint32_t riff_length = file_length - 8;
    fseek(file,4,SEEK_SET);
    fwrite(&riff_length, sizeof(riff_length), 1, file);

    fclose(file);
}

void WaveFileGenerator::setSampleGenerator(std::unique_ptr<AbstractSampleGenerator> generator)
{
    m_sample_generator = std::move(generator);
}
