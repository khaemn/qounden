#include "ntonesamplegenerator.h"

NToneSampleGenerator::NToneSampleGenerator(double frequency, double volume)
    : AbstractSampleGenerator(frequency, volume)
{
}

Samples::iterator
NToneSampleGenerator::fillWithSamples(Samples::iterator begin, unsigned int count)
{
    auto iter = begin;
    for (unsigned int i=0; i < count; i++) {
        const double t = static_cast<double>(i) / m_samples_per_sec;
        int16_t pcm_value = static_cast<int16_t>(
                    sin(m_obertone_frequency_hz * t * 2. * M_PI)
                    * m_bit_volume
                    );
        for (int tone = 1; tone <= m_obertones; tone++) {
            double ober_freq = m_obertone_frequency_hz * 2 * tone;
             pcm_value += static_cast<int16_t>(
                            sin( ober_freq * t * 2. * M_PI )
                            * (1. * m_bit_volume / ( 4. * tone * tone))
                             );
        }
        pcm_value += (drand48() * m_bit_volume / 128) - m_bit_volume / 256;
        *iter++ = pcm_value;
    }
    return iter;
}
