#ifndef RANDOMTONESAMPLEGENERATOR_H
#define RANDOMTONESAMPLEGENERATOR_H

#include <memory>

#include "abstractsamplegenerator.h"

class RandomToneSampleGenerator : public AbstractSampleGenerator
{
public:
    explicit RandomToneSampleGenerator(double frequency,
                                        double volume);

    virtual Samples getSamples(unsigned int count);
    virtual Samples::iterator fillWithSamples(Samples::iterator begin, unsigned int count) override;

private:
    std::unique_ptr<AbstractSampleGenerator> m_toner;
    std::unique_ptr<AbstractSampleGenerator> m_noiser;
};

#endif // RANDOMTONESAMPLEGENERATOR_H
