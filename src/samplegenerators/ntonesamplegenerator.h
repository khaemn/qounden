#ifndef NTONESAMPLEGENERATOR_H
#define NTONESAMPLEGENERATOR_H

#include "abstractsamplegenerator.h"

class NToneSampleGenerator : public AbstractSampleGenerator
{
public:
    explicit NToneSampleGenerator(double frequency,
                                  double volume);

    virtual Samples::iterator fillWithSamples(Samples::iterator begin, unsigned int count) override;

    // TODO: a setter
    unsigned int m_obertones = 4;
};

#endif // NTONESAMPLEGENERATOR_H
