#include "noisesamplegenerator.h"

#include <random>

NoiseSampleGenerator::NoiseSampleGenerator(double frequency, double volume)
    : AbstractSampleGenerator(frequency, volume)
{
}

Samples::iterator
NoiseSampleGenerator::fillWithSamples(Samples::iterator begin, unsigned int count)
{
    auto iter = begin;
    for(unsigned int i=0; i < count; i++) {
        *iter++ = m_bit_volume * drand48() * 2 - m_bit_volume;
    }
    return iter;
}
