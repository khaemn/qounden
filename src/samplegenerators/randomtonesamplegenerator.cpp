#include "randomtonesamplegenerator.h"

#include "ntonesamplegenerator.h"
#include "noisesamplegenerator.h"

RandomToneSampleGenerator::RandomToneSampleGenerator(double frequency,
                                                     double volume)
    : AbstractSampleGenerator(frequency, volume)
    , m_toner(new NToneSampleGenerator(frequency, volume))
    , m_noiser(new NoiseSampleGenerator(frequency, .05))
{
}

Samples RandomToneSampleGenerator::getSamples(unsigned int count)
{
    const unsigned int tone_samples = 6800;
    const unsigned int noise_samples = 400;
    const unsigned int total_samples_per_block = tone_samples + noise_samples;
    unsigned int total_blocks = count / total_samples_per_block; // TODO: make adjustable.
    unsigned int block_len = (count / total_blocks);

    Samples result(total_blocks * total_samples_per_block);
    auto itr = result.begin();

    for (unsigned int block = 0; block < total_blocks; ++block) {
        double freq = drand48() * m_obertone_frequency_hz;

        m_toner->setObertoneFrequency(freq);
        itr = m_noiser->fillWithSamples(itr, noise_samples);
        std::advance(itr, -10);
        itr = m_toner->fillWithSamples(itr, tone_samples + 10);
    }

    return result;
}

Samples::iterator
RandomToneSampleGenerator::fillWithSamples(Samples::iterator begin, unsigned int count)
{
    return begin;
}
