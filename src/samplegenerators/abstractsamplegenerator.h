#ifndef ABSTRACTSAMPLEGENERATOR_H
#define ABSTRACTSAMPLEGENERATOR_H

#include <iterator>
#include <vector>
#include <stdint.h>
#include <math.h>

using Samples = std::vector<int16_t>;

class AbstractSampleGenerator {

public:
    explicit AbstractSampleGenerator(double frequency, double volume);

    virtual ~AbstractSampleGenerator() = default;

    virtual Samples getSamples(unsigned int count) = 0;
    virtual Samples::iterator fillWithSamples(Samples::iterator begin,
                                              unsigned int count) {
        return begin;
    } // TODO: pure virtual

    double obertoneFrequency() const {
        return m_obertone_frequency_hz;
    }

    void setObertoneFrequency(double frequency_hz) {
        m_obertone_frequency_hz = frequency_hz;
    }

    double volume() const  {
        return m_volume;
    }

    void setVolume(double volume) {
        m_volume = volume;
    }

    unsigned int samplesPerSecond() const {
        return m_samples_per_sec;
    }

    void setSamplesPerSecond(unsigned int samples_per_sec) {
        m_samples_per_sec = samples_per_sec;
    }

    static const unsigned int DEFAULT_SAMPLES_PER_SECOND = 44100;
    static const unsigned int DEFAULT_BIT_DEPTH = 16;

protected:
    double m_obertone_frequency_hz = 0.;
    double m_volume = 1.0;
    unsigned int m_samples_per_sec = DEFAULT_SAMPLES_PER_SECOND;
    int m_bit_volume = static_cast<int>(pow(2, DEFAULT_BIT_DEPTH));
};


#endif // ABSTRACTSAMPLEGENERATOR_H
