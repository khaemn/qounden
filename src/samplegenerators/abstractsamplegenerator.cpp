#include "abstractsamplegenerator.h"

AbstractSampleGenerator::AbstractSampleGenerator(double frequency, double volume)
    : m_obertone_frequency_hz(frequency)
    , m_volume(volume)
    , m_bit_volume(static_cast<int>(pow(2, DEFAULT_BIT_DEPTH) * m_volume))
{
}

Samples AbstractSampleGenerator::getSamples(unsigned int count)
{
    Samples samples(count);
    fillWithSamples(samples.begin(), count);
    return samples;
}
