#ifndef SINEWAVESAMPLEGENERATOR_H
#define SINEWAVESAMPLEGENERATOR_H

#include "abstractsamplegenerator.h"

class SineWaveSampleGenerator : public AbstractSampleGenerator
{
public:
    explicit SineWaveSampleGenerator(double frequency,
                                     double volume);

    virtual Samples getSamples(unsigned int count) override;
};

#endif // SINEWAVESAMPLEGENERATOR_H
