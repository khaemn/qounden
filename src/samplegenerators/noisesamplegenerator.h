#ifndef NOISESAMPLEGENERATOR_H
#define NOISESAMPLEGENERATOR_H

#include "abstractsamplegenerator.h"

class NoiseSampleGenerator : public AbstractSampleGenerator
{
public:
    explicit NoiseSampleGenerator(double frequency,
                                  double volume);

    virtual Samples::iterator fillWithSamples(Samples::iterator begin, unsigned int count);
};

#endif // NOISESAMPLEGENERATOR_H
