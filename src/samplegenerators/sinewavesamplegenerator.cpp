#include "sinewavesamplegenerator.h"

SineWaveSampleGenerator::SineWaveSampleGenerator(double frequency, double volume)
    : AbstractSampleGenerator(frequency, volume)
{
}

Samples SineWaveSampleGenerator::getSamples(unsigned int count)
{
    Samples samples;
    samples.reserve(count);
    for (unsigned int i=0; i < count; i++) {
        const double t = static_cast<double>(i) / m_samples_per_sec;
        const int16_t pcm_value = static_cast<int16_t>(
                    sin(m_obertone_frequency_hz * t * 2 * M_PI)
                    * m_bit_volume);
        samples.emplace_back(pcm_value);
    }
    return samples;
}
