TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += libsrc/wavfile/ src/

SOURCES += \
    main.cpp \
    src/samplegenerators/sinewavesamplegenerator.cpp \
    src/samplegenerators/abstractsamplegenerator.cpp \
    src/samplegenerators/ntonesamplegenerator.cpp \
    src/samplegenerators/noisesamplegenerator.cpp \
    src/samplegenerators/randomtonesamplegenerator.cpp \
    src/wavefilegenerator.cpp

HEADERS += \
    src/samplegenerators/abstractsamplegenerator.h \
    src/samplegenerators/sinewavesamplegenerator.h \
    src/samplegenerators/ntonesamplegenerator.h \
    src/samplegenerators/noisesamplegenerator.h \
    src/samplegenerators/randomtonesamplegenerator.h \
    src/wavefilegenerator.h
